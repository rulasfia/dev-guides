# Kubb Review

### Pros
- Auto generate Typescript definition untuk setiap endpoint, meliputi request body, url params, dan response body. 
- Banyak opsi plugin, termasuk untuk Tanstack Query. Hasil kode yang digenerasi berupa hooks yang bisa langsung dipakai di component.

### Const
- Swagger / OpenAPI specification yang dibuat oleh backend menjadi _source of truth_ utama. Artinya backend harus tersedia terlebih dahulu (dengan openAPI spec yang tepat) sebelum frontend bisa melakukan integrasi.
- Masih relatif baru dan belum terlalu populer (<500 github stars). Sehingga masih jarang artikel, tutorial, atau topik terkait di stackoverflow.
- Ketika melakukan tuning pada file hasil generate kubb secara langsung, perubahan tersebut akan ter-override tiap perintah `generate` dijalankan.
- Hasil kode yang digenerate relatif kompleks, terutama jika menggunakan plugin untuk Tanstack Query. 

### Not Sure Yet
- Handle multiple environment dengan API yang berbeda (local, dev, test, prod). Karena kode yang digenerate diperlukan saat runtime, artinya proses code generation harus berjalan setiap sebelum aplikasi di build.
- Tingkat kustomisasi / Tuning hasil generated code.

### Alternative Idea
Kita bisa menggunakan Typescript definition yang digenerate oleh Kubb, kemudian menggunakannya secara manual untuk custom hooks Tanstack Query. Dengan cara ini kita akan
- Mendapatkan kemudahan tidak perlu secara manual membuat definition untuk tiap endpoint
- Tetap menjadikan OpenAPI specification yang dihasilkan backend sebagai patokan. 
- Tidak perlu khawatir terkait sedikitnya tutorial terkait atau kompleksitas generated code yang dihasilkan. 
- Karena TS tidak berjalan saat runtime, maka tidak perlu khawatir hasil generated kode dari kubb mempengaruhi jalannya aplikasi.

### Conclusion

It's quite good and I think if we can make some adjustment to the generated code (directly or indirectly), this might help us work on our integration faster & safer.