# UCP Web Refactor - Dev Guides

**Table of Content:** 

[[_TOC_]]

## Requirement and Setup

### Runtime Requirement

Versi minimum yang diperlukan:

- Node: >= 20.10
- NPM: >= 9 (for lockfile v3)

### Code Editor Setup

Sesuai dengan base template terbaru, project ini mempunyai aturan untuk linting dan code formatting. Sehingga perlu diinstall beberapa code editor extension untuk merasakan DX maksimal, termasuk deteksi error lebih awal, standarisasi code, serta format code yang rapi dan jelas. Hal-hal tersebut akan sangat bermanfaat untuk project jangka panjang dan dikerjakan oleh lebih dari satu developer.

Berikut extension yang perlu diinstall (asumsi menggunakan VSCode):

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [CSS Module](https://marketplace.visualstudio.com/items?itemName=clinyong.vscode-css-modules) (Optional)

Selain extension tersebut, ada beberapa settings yang perlu dirubah. untuk merubah setting di VSCode dapat dilakukan dengan menekan `ctrl + shift + p`, lalu cari "user settings json".

Lalu tambahkan beris berikut pada paling bawah.

```javascript
{
  "editor.defaultFormatter": "esbenp.prettier-vscode", // this is required
  "editor.formatOnSave": true // optional but recommended
}
```

### Base Template - Cleanup

Template yang akan digunakan untuk refactor kedepannya adalah repository `BaseTypescript` pada branch `v_1` (Jan 2024). Template tersebut datang dengan banyak konfigurasi dan konvensi bawaan yang sangat berguna untuk membiasakan diri dengan konvensi yang digunakan. Namun ketika mulai mengerjakan project sesungguhnya, akan banyak bawaan dari template tersebut yang belum akan kita gunakan.

Berikut beberapa hal yang bisa dilakukan untuk menyederhakan template tersebut untuk memulai project baru.

```sh
src/
+-- api #SAFE_TO_REMOVE -> shared code to communicate to the server
+-- components #SAFE_TO_REMOVE -> shared components used across the entire app
+-- config #KEEP -> shared config files for various purpose
+-- constant #KEEP -> as it says
+-- hooks #KEEP -> shared hooks used across the entire app
+-- mocks #SAFE_TO_REMOVE -> setup msw for testing & server mocking
+-- pages #SAFE_TO_REMOVE -> shared pages used across the entire app
+-- queries #SAFE_TO_REMOVE -> no ideas
+-- routes #KEEP -> all of the app routes
+-- theme #SAFE_TO_REMOVE -> shared MUI theme
+-- types #KEEP -> shared global type or schema
+-- utils #KEEP -> shared helpers used across the entire app
```

Beberapa perubahan juga diperlukan pada beberapa file agar project bisa dijalankan, beberapa diantaranya adalah:

- [`package.json`](./package.json)
- [`.prettierrc.js`](./.prettierrc.cjs)
- [`App.tsx`](./src/App.tsx)
- [`/routes/router.tsx`](./src/routes/router.tsx)

### Extra (Optional)

Hal berikutnya yang bisa dilakukan adalah menginstall beberapa plugin untuk prettier. Ini optional, namun dapat berguna untuk membuat kodebase menjadi lebih konsisten dan lebih mudah dibaca dalam jangka panjang.

Plugin tambahan yang disarankan disini adalah plugin untuk auto sorting import oleh [ianVs](https://github.com/IanVS/prettier-plugin-sort-imports). Langkah pertama adalah install terlebih dahulu.

```shell
npm install --save-dev @ianvs/prettier-plugin-sort-imports
```

Kemudian update konfigurasi `.prettierrc.cjs` dengan baris berikut, lalu restart VSCode.
```javascript
module.exports = {
  // ...
  plugins: ['@ianvs/prettier-plugin-sort-imports'],
  importOrder: ['^react', '<THIRD_PARTY_MODULES>', '', '^@/(.*)$', '^[./]'],
}
```

# Convention & Standard

## Routing

Sejak awal kemunculannya, [React Router](https://reactrouter.com/en/main) telah menjadi pilihan utama yang mudah dan reliable bagi project SPA yang ingin memiliki beberapa route. Versi package `react-router-dom` yang akan digunakan adalah >= 6.4.

Di versi 6 keatas ini, React Router menghadirkan cukup banyak perubahan serta tambahan fitur-fitur baru yang sangat menarik. Berikut beberapa fitur-fitur kunci yang akan diimplementasikan pada project ini.

### Nested Route (Layout Route)

Bayangkan sebuah website dengan dua level navigasi. Level pertama adalah navigasi pada Header. Header disini akan selalu tampil dan tidak berubah pada halaman apapun. Lalu ditiap halaman akan ada navigasi level kedua pada sidebar yang hanya akan berubah sesuai navigasi level pertama.

![nested router example](assets/nested_router.png "Nested router example"){width=72%}

Case tersebut adalah contoh sempurna untuk menggunakan patern nested routing. Daftar route yang dihasilkan dari ilustrasi tersebut yaitu:
- `/`
- `/menu-one`
- `/menu-one/one-b`
- `/menu-one/one-c`
- `...`
- `/menu-two`
- `/menu-two/two-a`
- `...`

**Nested routing** adalah konsep dimana hanya sebagian dari website yang berubah untuk merespon perubahan URL. Berdasarkan contoh diatas, ketika yang berubah hanya url di segment kedua (`one-c`), maka element dari url segment sebelumnya (header & sidebar) tidak perlu ikut berubah. 

![nested routing url segment](assets/url_segment.png "Nested routing URL segment"){width=65%}

Pada react router, implementasi konsep ini dapat dibuat menggunakan kombinasi `createBrowserRouter`, `children` property, serta `<Outlet />` component. Berikut contoh implementasi konfigurasi router untuk contoh diatas (_simplified_).

```tsx
// router.tsx

export const router = createBrowserRouter([
  {
    // `/` (root segment)
    path: "/",
    // element: Header and <Outlet /> for child element,
    element: <></>,
    children: [
      {
        // `/menu-one` (segment one route)
        path: "menu-one",
        // element: Sidebar and <Outlet /> for child element
        element: <></>,
        children: [
          {
            // `/menu-one/one-b` (segment two route)
            path: "one-b",
            // element: content for /menu-one/one-b
            element: <></>,
          },
        ],
      },
      {
        // `/menu-two` (segment one route)
        path: "menu-two",
        // element: Sidebar and <Outlet /> for it's child element
        element: <></>,
      },
    ],
  },
]);
```

### Data Loader

**Data Loader** pada react router pada dasarnya hanyalah function yang berjalan sebelum component pada sebuah route dirender. Tujuan utama dari fitur ini sebenarnya adalah untuk menghubungi API untuk me-request data yang diperlukan pada route terkait. 

Namun pada project refactor ini implementasi data loader hanyalah sebatas untuk memproteksi route yang memperlukan akses authorization, karena untuk berkomunikasi dengan API akan menggunakan library lain yang lebih sesuai. Sehingga yang akan terjadi adalah ketika user mencoba mengakses route yang terproteksi, _loader function_ akan berjalan untuk memastikan user tersebut mempunyai akses yang sesuai. Jika memang memiliki akses, component untuk route terkait baru akan dirender.

Contoh implementasinya kurang lebih adalah seperti berikut (_simplified_).
```tsx
// router.tsx

// simplified example
function checkAuth() {
  const isAuthenticated = localStorage.get('token')
  if (isAuthenticated) return null
  
  return redirect(`/login`);
}

export const router = createBrowserRouter([
  {
    path: "/login",
    element: <></>,
  },
  {
    path: "/dashboard",
    // set loader to check-auth function in a protected route
    loader: checkAuth, 
    element: <></>,
  }
]);
```

### Folder & File Structures (optional)

Karena disini menggunakan file konfigurasi untuk mendeklarasikan routes pada aplikasi, maka sebenarnya tidak ada aturan mengikat terkait bagaimana menamai atau menempatkan sebuah route. Ini artinya jika ingin menempatkan file konten route langsung di `src`, lalu kemudian diimport di `route.tsx`, aplikasi masih bisa berjalan dengan normal.

Ini baru akan menimbulkan masalah ketika semakin besar aplikasi berkembang dan semakin banyak route yang dimiliki. Bagaimana cara mengorganisir file dan kode yang digunakan juga menjadi jauh lebih pentimg, terlebih untuk project yang dikerjakan oleh beberapa developer sekaligus. Sehingga keputusan yang lebih bijak adalah membuat sebuah konvensi atau aturan terkait bagaimana mengorganisir nama file dan lokasi kode yang saling terkait. 

Konvensi untuk routing yang akan digunakan disini terinspirasi dari penamaan file-based-routing pada framework [Remix](https://remix.run/docs/en/main/discussion/routes#conventional-route-folders) yang menggunakan _dot_ (`.`) sebagai pemisah antar segment url, _underscore_ (`_`) untuk route index dan layout, serta _dollarsign_ (`$`) untuk dynamic route variable. Namun dengan sedikit dimodifikasi dengan membuat folder untuk mengelompokkan berdasarkan url segment pertama. Contoh sederhananya adalah seperti berikut

| URL | File Path | Has Layout | Layout Path |
| --- | --- | --- | --- |
| `/`   | `pages/_index/_index.tsx` | `true` | `pages/_index/_root.tsx`   |
| `/m-one`   | `pages/m-one/m-one._index.tsx` | `false` | nearest `._root.tsx`   |
| `/m-two`   | `pages/m-two/m-two._index.tsx` | `true` | `pages/m-two/m-two._root.tsx`   |
| `/m-two/two-b`   | `pages/m-two/m-two.two-b._index.tsx` | `false` | nearest `._root.tsx`   |
| `/m-five/:id`   | `pages/m-five/m-five.$id._index.tsx` | `false` | nearest `._root.tsx`   |

## Component Composition

### Atomic Design

Atomic Design adalah metodologi untuk membuat design system dengan membagi element UI menjadi component-component kecil yang saling tersusun. Terdapat 5 level kompleksitas pada atomic design, dari paling sederhana hingga kompleks.

1. Atom
2. Molecule
3. Organism
4. Template
5. Pages

![Atomic design](/assets/atomic_design.png "Atomic design"){width=65%}

Sebagai contoh sederhana, bayangkan kita akan membua halaman (`Pages`) seperti berikut.
![Atomic design - pages](/assets/atomic_pages.png "Atomic design - pages"){width=70%}

Beberapa pembagian element yang bisa kita lakukan dari contoh tersebut adalah.
- Atoms: Button, Input dan NavLink.
- Molecules: NewPost, PostCard, dan Trending component.
- Organisms: PostList, LeftSidebar, dan RightSidebar component.
- Templates: CenterLayout component.
- Pages: Timeline page.

Pada prakteknya, kita bisa membuat folder untuk mengemlompokan atoms, molecules, dan organisms pada `src/components/`. Sedangkan untuk templates dan pages akan berada pada `src/pages/`.

```sh
src/
+-- components/
+----- atoms/ 
+----- molecules/
+----- organism/
+-- pages/
+----- _index.tsx   # example for pages 
+----- _root.tsx    # example for template. 
                    # any _root.tsx component is essentially valid template 
```

### Prefer Composition

Secara sederhana dapat dilihat pada illustrasi dibawah ini (credit to [George Moller](https://twitter.com/_georgemoller/status/1740206017017679882/photo/1)):

![Prefer composition](assets/prefer_composition.jpeg "Prefer composition"){width=65%}

## Styling & CSS

Untuk styling kita akan memanfaatkan vanilla CSS dengan bantuan CSS module untuk scoped style per-component. Dengan adanya fitur-fitur CSS modern seperti [variable](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties), [nesting](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_nesting/Using_CSS_nesting), [container query](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_container_queries), dll, tidak banyak _usecase_ yang memerlukan css preprocesor seperti SCSS atau LESS. 

Ada beberapa langkah yang perlu dilakukan untuk persiapan:

### Setup LightningCSS
By default, Vite menggunakan PostCSS untuk memproses CSS. Kekurangannya disini adalah, kita perlu menginstall package tersendiri untuk `autoprefixer` dan `import`. 

Sehingga kita akan menggunakan tool alternatif yaitu [LightningCSS](https://lightningcss.dev/), yang sudah mempunyai fitur-fitur tersebut secara build-in. LightningCSS juga memungkinkan untuk menulis CSS dengan fitur-fitur yang paling baru tanpa perlu khawatir terkait kompatibilitas dengan browser versi lama karena akan dihandle secara [otomatis](https://lightningcss.dev/transpilation.html).

Selain itu, LightningCSS juga lebih unggul dari sisi kecepatan dalam parsing dan minifying css kita.  

Pertama, install terlebih dahulu package LightningCSS
```shell
npm install --D lightningcss
```

Lalu update konfigurasi `css` dan `build` pada `vite.config.ts`
```typescript
// vite.config.ts

{
  // ...
  css: {
    transformer: "lightningcss",
    // ...
  },
  build: {
    cssMinify: "lightningcss",
    cssCodeSplit: false,
    // ...
  }
}
```

### Setup CSS Reset
Buat file baru pada `src/reset.css`, kemudian isi dengan css reset yang telah disediakan. Ini digunakan agar styling tiap element menjadi lebih konsisten dengan styling default yang lebih rasional. 

> Berikut referensi untuk [CSS Reset](./src/reset.css)

CSS reset tersebut terinspirasi dari [blog post](https://piccalil.li/blog/a-more-modern-css-reset/) dari Andy Piccalilli. Disitu juga telah disertakan alasan mengapa tiap attribute pada reset tersebut diterapkan.

Kemudian import style tersebut pada `src/index.css` pada line paling atas.

```css
/* index.css */

@import './reset.css';
```

Jangan lupa import juga file `index.css` di `App.tsx`
```tsx
// App.tsx

import './index.css';
```

### Design Token with CSS Variable

Design token adalah perwakilan keputusan desain yang diterjemahkan sebagai data. Ini pada dasarnya adalah alat komunikasi, bahasa yang sama yang dimengerti oleh designer dan developer yang menyusun sebuah desain system.

Contoh dari design token disini misalnya varian warna yang digunakan, detail typography, aturan spasi, dll. Pada project ini, nilai dari tiap design token akan disimpan menggunakan CSS Variable yang dapat diakses dari file CSS di seluruh aplikasi, termasuk dari dalam scoped CSS atau CSS module.

Untuk menyimpan variable design token, perlu disiapkan file tersendiri yaitu `src/token.css`, yang kemudian akan diimport di `src/index.css`

```css
/* token.css */

:root {
  --font-size-h1: 56px;
  --line-height-h1: 64px;
  --letter-space-h1: calc(var(--font-size-h1) * (-0.01));

  --color-primary-base: hsla(228, 96%, 60%, 1);
  --color-state-error: hsla(349, 78%, 49%, 1);
  /* ... */
}
```
```css
/* index.css */

@import './reset.css';
@import './token.css';
```

Kemudian dapat digunakan seperti CSS Variable pada umumnya.

```css
/* button.module.css */

.primary {
  background-color: var(--color-primary-base);
}

.danger {
  background-color: var(--color-state-error);
}
```

### CSS Module

CSS Module digunakan untuk scoping css hanya pada file yang meng-import. Ini sangat cocok dipasangkan dengan proses development yang menggunakan component based karena dapat menghindari potensi konflik dari global style. 

Untuk menggunakan CSS Module, cukup buat satu file dengan nama yang sama dengan component yang akan di style, namun dengan extensi `.module.css`. Kemudian import default pada file component yang akan menggunakannya. 

```sh
components/atoms/button
+-- button.tsx 
+-- button.module.css
```
```css
/* button.module.css */

.primary {
  background-color: var(--color-primary-base);
}

.danger {
  background-color: var(--color-state-error);
}
```
```tsx
// button.tsx

import styles from './button.module.css';

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  variant?: 'primary' | 'danger';
};

export function Button({ variant = 'primary', ...props }: ButtonProps) {
  const cn = variant === 'primary' ? styles.primary : styles.danger

  return <button className={cn} {...props}>Example</button>
}

```

### Responsive Design

Berikut standard breakpoint yang akan digunakan (referensi breakpoint yang digunakan [tailwindcss](https://tailwindcss.com/docs/screens)):
- `640px` (sm)
- `768px` (md)
- `1024px` (lg)
- `1280px` (xl)

Pada tiap breakpoint tersebut, nilai `max-width` untuk content nya juga diatur sesuai breakpoint terkait. Sehingga content tidak akan stretch dan tetap terlihat rapi di antara breakpoint manapun. 

Contoh kasus: Ketika user melihat website pada screen width 1200px, maka max-width konten yang dilihat oleh user adalah 1024px dengan space tersisa (176px) berada di sisi kiri dan kanan konten.

```css
/* index.css */
/* ... */

.container {
  width: 100%;
  padding: 16px;
  margin: 0 auto;

  @media (min-width: 640px) {
    max-width: 640px;
    padding: 0;
  }
  @media (min-width: 768px) {
    max-width: 768px;
    padding: 0;
  }
  @media (min-width: 1024px) {
    max-width: 1024px;
    padding: 0;
  }
  @media (min-width: 1280px) {
    max-width: 1280px;
    padding: 0;
  }
}

```

Metode "mobile first approach" lebih diutamakan. Artinya class / property css akan mereferensi untuk breakpoint paling kecil, kemudian untuk screen lebih besar ditaruh di dalam `@media` query. 

```css
.card {
  padding: 12px;

  @media (min-width: 640px) {
    padding: 16px;
  }

  @media (min-width: 1280px) {
    padding: 24px;
  }
}
```

## State Management

no redux no problem.

- local state -> `useState`
- regional(?) state -> `useContext`
- global state -> url search param 
- global state (persistent) -> localStorage

## API Management

### About Kubb

Reference: 
- [Kubb Documentation](https://www.kubb.dev/)
- [Kubb Github Repo](https://github.com/kubb-project/kubb)
- [Review](./kubb-review.md)
- [My Research on This](https://rulasfia.notion.site/Explore-Kubb-Library-639f80d95bea4ed49998e05885602772)

Penjelasan terkait apa itu kubb, apa fungsinya, dan bagaimana dia bekerja terdapat pada link-link diatas. Artikel ini hanya akan berisi bagaimana kita menggunakan library ini pada project kita.

### Preparing Swagger.json File

Beberapa catatan yang perlu diingat antara lain adalah seperti berikut:
- Kubb menggunakan file [OpenAPI Specification](https://swagger.io/specification/) (`swagger.json`) sebagai pedoman untuk kode yang akan di-_generate_.
- Struktur file yang akan dipisah _based on_ service.
- Karena tidak semua API pada suatu service akan digunakan, maka perlu membuat local copy dari `swagger.json` service tersebut, dengan hanya berisi API yang terpakai.

Berikut implementasi yang akan kita terapkan. Sebagai contoh, kita akan menggunakan API `/api/Promotion/imageslider` dari promotion service.

1. Buka file `swagger.json` dari service tersebut, copy semuanya dan paste ke new tab di code editor. 

![kubb guide 1](assets/kubb_1.png "kubb guide 1"){width=60%}
![kubb guide 2](assets/kubb_2.png "kubb guide 2"){width=60%}

2. Buat file baru dengan nama yang menyesuaikan service yang akan dibuat pada `/swagger/{service-name.json}`. Untuk contoh kasus ini, file yang dimaksud adalah `promotion.json`.

![kubb guide 3](assets/kubb_3.png "kubb guide 3"){width=30%}

3. Copy semua content yang kita dapatkan dari step 1 ke file `promotion.json` yang baru dibuat, kecuali untuk attribute `path` dan `components.schema`

![kubb guide 4](assets/kubb_4.png "kubb guide 4"){width=56%}

4. Berikutnya copy `path` dari step 1 ke file `promotion.json`. Disini, kita hanya perlu mengambil `path` dari endpoint yang kita butuhkan. Pada contoh ini, endpoint yang dibutuhkan adalah `/api/Promotion/imageslider`

![kubb guide 5](assets/kubb_5.png "kubb guide 5"){width=56%}

5. Jika dilihat pada `path` diatas, akan terlihat terdapat property `"$ref"` `responses` atrribute. `$ref` disini adalah schema yang kita butuhkan untuk endpoint ini. Sehingga kita perlu mencari dan menyalin isinya ke file `promotion.json` yang kita miliki. Tips: `ctrl + click` di vscode untuk langsung menuju schema yang dimaksud.

![kubb guide 6](assets/kubb_6.png "kubb guide 6"){width=50%}
![kubb guide 7](assets/kubb_7.png "kubb guide 7"){width=50%}

### Creating Service Instance

Pada implementasi kita, selain mempunyai swagger.json sendiri, tiap service juga akan memiliki instance-nya masing-masing. Ini akan digunakan oleh file hasil generate kubb untuk menghubungi API.

Jika menyesuaikan contoh kasus yang sama dengan sebelumnya, maka perlu dibuat instance baru untuk promotion service.

Buat file baru: `src/config/api.types.ts` (abaikan jika sudah ada). File ini akan berisi custom `AxiosInstance` yang digunakan pada instance service yang akan dibua
t.

```typescript
// src/config/api.types.ts

import {
  type Axios,
  type AxiosDefaults,
  type AxiosHeaderValue,
  type AxiosRequestConfig,
  type AxiosResponse,
  type HeadersDefaults,
} from 'axios';

export interface CustomAxiosInstance extends Axios {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  <T = unknown, _R = unknown, D = unknown>(
    config: AxiosRequestConfig<D>,
  ): Promise<AxiosResponse<T>>;

  defaults: Omit<AxiosDefaults, 'headers'> & {
    headers: HeadersDefaults & {
      [key: string]: AxiosHeaderValue;
    };
  };
}
```

Kemudian buat lagi file baru berisi axios instance untuk promotion service pada `src/config/promotionInstance.ts`. Disini header/property/url yang digunakan dapat disesuaikan tergantung service terkait.
```typescript
import { type AxiosStatic } from 'axios';

import apiConfig from '@/config/api';
import { type CustomAxiosInstance } from './api.types';

const promotionInstance = (apiConfig as AxiosStatic).create({
  baseURL: `${import.meta.env.REACT_APP_API_URL}/ucp/promotion`,
  headers: {
    Accept: 'application/json,application/pdf,application/octet-stream',
    'Ocp-Apim-Subscription-Key': import.meta.env.REACT_APP_OCP_APIM_KEY,
  },
});

export default promotionInstance as CustomAxiosInstance;
```

Pada file tersebut kita menggunakan `CustomAxiosInstance` yang sebelumnya dibuat sebagai _type assertion_ pada `promotionInstance` yang diexport. File ini nantinya akan digunakan oleh kubb pada file hasil generate untuk menghubungi service kita.

### Kubb Generate 

Langkah berikutnya adalah menjalankan command dari kubb untuk menggenerate kode sesuai dengan `swagger.json` dan service instance yang baru saja kita buat.

Sebelum itu, kita perlu melakukan update pada file `kubb.config.json` agar mendapatkan output kode yang lebih sederhana dan sesuai keinginan.

```typescript
// kubb.config.ts
import { defineConfig } from '@kubb/core';
import { definePlugin as createSwagger } from '@kubb/swagger';
import createSwaggerFaker from '@kubb/swagger-faker';
import createSwaggerMsw from '@kubb/swagger-msw';
import createSwaggerTanstackQuery from '@kubb/swagger-tanstack-query';
import createSwaggerTS from '@kubb/swagger-ts';
import createSwaggerZod from '@kubb/swagger-zod';

export default defineConfig(() => {
  const tanstackOptions: Parameters<typeof createSwaggerTanstackQuery>[0] = {
    output: { path: 'hooks', exportType: false },
    group: { type: 'tag', output: 'hooks/{{tag}}Hooks' },
    dataReturnType: 'data',
    transformers: {
      name: (name, type) => {
        if (type === 'file' || type === 'function') {
          return `${name}Hook`;
        }
        return name;
      },
    },
  };

  const plugins = [
    createSwagger({ output: false }),
    createSwaggerTS({
      output: { path: 'models', exportType: false },
      group: { type: 'tag', output: 'models/{{tag}}Models' },
    }),
    createSwaggerMsw({
      output: { path: 'msw', exportType: false },
      group: { type: 'tag', output: 'msw/{{tag}}Handlers' },
    }),
    createSwaggerFaker({
      output: { path: 'mocks', exportType: false },
      group: { type: 'tag', output: 'mocks/{{tag}}Mocks' },
    }),
    createSwaggerZod({
      output: { path: 'schemas', exportType: false },
      group: { type: 'tag', output: 'schemas/{{tag}}Schemas' },
    }),
  ];

  return [];
});
```

Diatas adalah konfigurasi umum kubb yang akan digunakan oleh semua service instance. Berikutnya adalah menambahkan konfigurasi spesifik untuk tiap-tiap service. Jika menyesuaikan contoh promotion service sebelumnya, berikut konfigurasi yang perlu ditambahkan pada `kubb.config.ts`

```typescript
// kubb.config.ts

export default defineConfig(() => {
  // ....

  // from:
  // return []
  
  // update to:
  return [
    {
      /* service name */
      name: 'promotion', 
      root: '.',
      /* swagger.json location */ }, 
      input: { path: './swagger/promotion.json' 
      hooks: { done: ['prettier --write "src/api/promotion/**/*.{ts,tsx}"'] },
      /* output location */,
      output: { path: './src/api/promotion', clean: true } 
      plugins: [
        ...plugins,
        createSwaggerTanstackQuery({
          ...tanstackOptions,
          /* instance location */
          client: { importPath: '@/config/promotionInstance' },
        }),
      ],
    },
  ] 
}
```

Ketika file konfigurasi `kubb.config.ts` telah terupdate dengan konfigurasi service yang diperlukan, langkah berikutnya adalah menjalankan perintah generate.

```shell
npm run generate-api
```

Hasil generate dari perintah tersebut akan menyesuaikan konfigurasi yang dibuat (by default pada `src/api`)

### Tanstack Query

Untuk menggunakan file hasil generate pada step sebelumnya, bisa langsung import hooks dari file yang digenerate oleh kubb ke component yang memerlukan data tersebut. 

```typescript
import { PromotionImageSliderDto } from '@/api/promotion/models/PromotionImageSliderDto';

export function PromotionSection() {
  const { data, status } = usePromotionGetImageSliderForDeviceHook();

  if (status === 'pending') return <span>Loading</span>
  if (status === 'error') return <span>Something wrong!</span>

  return <span>{JSON.stringify(data)}</span>
}
```

## Syntax

and others (in progress)
