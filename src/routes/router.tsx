import { createBrowserRouter, Outlet } from 'react-router-dom';

import { BASE_ROUTE } from '@/constants/routes';
import { readyCheckerLoader } from '@/routes/loader';

const router = createBrowserRouter(
	[
		{
			path: '*',
			element: <span>Not Found!</span>,
		},
		{
			id: 'root',
			path: BASE_ROUTE,
			loader: readyCheckerLoader,
			element: (
				<div>
					<p>Shared Element</p>
					<Outlet />
				</div>
			),
			children: [
				{
					index: true,
					element: <p>Hello World</p>,
				},
			],
		},
	],
	{ basename: import.meta.env.DEV ? undefined : '/portalsvc' },
);

export default router;
