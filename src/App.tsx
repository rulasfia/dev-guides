import { RouterProvider } from 'react-router-dom';
import NiceModal from '@ebay/nice-modal-react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';

import router from './routes/router';

const queryClient = new QueryClient();

export default function App() {
	return (
		<QueryClientProvider client={queryClient}>
			<NiceModal.Provider>
				{import.meta.env.DEV ? (
					<ReactQueryDevtools initialIsOpen={false} />
				) : null}
				<RouterProvider
					router={router}
					fallbackElement={<span>Loading...</span>}
				/>
			</NiceModal.Provider>
		</QueryClientProvider>
	);
}
